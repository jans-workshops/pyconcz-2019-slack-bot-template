#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Standalone lambda function triggered by CWE.
"""
import datetime as dt
import json
import logging

from slack import WebClient

from standup_bot.config import (
    SLACK_TOKEN,
    SLACK_CHANNEL,
    QUESTIONS,
)
from standup_bot.models import Report
from standup_bot.msg_templates import standup_menu_block, report_block

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# synchronous slack client
SC = WebClient(SLACK_TOKEN)


# ##### questions ###########
def send_menu(user_id, menu_block):
    """Send menu as private message to the user."""
    pass


def send_menus(menu_block):
    """Send menu to all users from the channel."""
    pass


def send_questions(report_id):
    """Entry point for daily menu."""
    pass


# ######## report ##############
def one_report(report):
    """Show report of one user."""
    pass


def all_reports(report_id):
    """Show reports of all users."""
    pass


def send_report(report_id):
    """Entry point for sending reports."""
    pass


# ############ main handler #####################

def lambda_handler(lambda_event, lambda_context):
    """Main lambda handler"""
    LOGGER.debug(lambda_context)
    LOGGER.info(lambda_event)
    LOGGER.info("lambda_event starts:")
    LOGGER.info(json.dumps(lambda_event))

    # is it our CWE event?
    if {"type", "time", "source"}.issubset(lambda_event):

        report_id = dt.datetime.strptime(lambda_event["time"], '%Y-%m-%dT%H:%M:%S%z').strftime("%Y%m%d")
        LOGGER.info("Report ID: %s", report_id)
        if lambda_event["type"] == "send_report":
            send_report(report_id)
        elif lambda_event["type"] == "send_questions":
            send_questions(report_id)
