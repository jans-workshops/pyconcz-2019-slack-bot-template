"""Service config."""

import os

SLACK_CHANNEL = os.environ.get("SLACK_CHANNEL")
SLACK_TOKEN = os.environ.get("SLACK_TOKEN")
TABLE_NAME = os.environ.get("DYNAMODB_TABLE")
AWS_REGION = os.environ.get("AWS_REGION")

questions = [
    "What did you work on yesterday?",
    "What is your plan for today?",
    "Any impediments?",
]

QUESTIONS = dict(
        (f"question{idx}", val) for idx, val in enumerate(questions)
    )
