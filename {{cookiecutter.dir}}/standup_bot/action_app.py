"""
Main flask app file used to receive incoming http requests from Slack.
"""
import logging
from urllib import parse

from flask import Flask, request, json, make_response
from slack import WebClient

from standup_bot.config import QUESTIONS, SLACK_TOKEN
from standup_bot.models import Report
from standup_bot.msg_templates import dialog_questions

# synchronous slack client
SC = WebClient(SLACK_TOKEN)

app = Flask(__name__)
app.config["SECRET_KEY"] = "you-will-never-guess"
app.logger.setLevel(logging.INFO)


@app.route("/actions", methods=["POST"])
def actions():
    """
    Endpoint /actions to process actions and dialogs.

    This method receives a request from API gateway.
    Example request: example-data/apigw-block_action.json

    We need to decode body and decide if requests is one of:
    - block_actions -> will trigger process_block_actions()
    - dialog_submission -> will trigger process_dialogs()

    Returns
    -------
    flask.Response

    """
    pass


def process_block_actions(slack_request: dict):
    """
    Slack Action processor.

    Here we are going to process decoded slack request "block actions"
    https://api.slack.com/reference/messaging/blocks#actions

    Example request: example-data/block_action.json

    We will present user with 2 buttons.
    1. Open dialog - which contains standup questions
    2. Skip today - to let user pass the meeting

    Returns
    -------
    flask.Response
        Empty response 200 signifies success.

    """
    action = slack_request["actions"][0]
    state_data = {"container": slack_request["container"], "report_id": action["value"]}
    if action["action_id"] == "standup.action.open_dialog":
        # Create and open slack dialog
        # code here
        return make_response()

    if action["action_id"] == "standup.action.skip_today":
        # Update the menu with message "See you tomorrow then."
        # your code here
        return make_response()

    return make_response("Unable to process action", 400)


def process_dialogs(slack_dialog: dict):
    """
    Process Slack dialogs.

    Here we are going to collect data from dialog

    example dialog submission: example-data/dialog_submission.json

    Returns
    -------
    flask.Response
        Successful dialog submission requires empty response 200.
    """
    if slack_dialog["callback_id"] == "standup.action.answers":
        # Create DB record.
        # Respond to slack user with message.

        # your code here
        return make_response()

    return make_response("Unable to process dialog", 400)
